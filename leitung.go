package werkbank_httpclient

import (
	"net"
	"net/http"
	"time"
)

type Leitung struct {
	CommonHeaders http.Header
	client        *http.Client
	Config
}

type Config struct {
	MaxIdleConnections int
	ConnectionTimeout  time.Duration
	ResponseTimeout    time.Duration
}

func New(cfg Config) Leitung {
	if cfg.MaxIdleConnections < 5 {
		cfg.MaxIdleConnections = 5
	}
	if cfg.ConnectionTimeout <= (0 * time.Second) {
		cfg.ConnectionTimeout = 2 * time.Second
	}
	if cfg.ResponseTimeout <= (0 * time.Second) {
		cfg.ResponseTimeout = 2 * time.Second
	}
	leitung := Leitung{
		Config: cfg,
	}

	client := http.Client{
		//Timeout: cfg.ConnectionTimeout * cfg.ResponseTimeout,
		Transport: &http.Transport{
			MaxIdleConns:          cfg.MaxIdleConnections,
			ResponseHeaderTimeout: cfg.ResponseTimeout,
			DialContext:           (&net.Dialer{Timeout: cfg.ConnectionTimeout}).DialContext,
		},
	}
	leitung.client = &client

	return leitung
}

func (l *Leitung) SetCommonHeaders(headers http.Header) {
	l.CommonHeaders = headers
}

func (l *Leitung) Get(url string, headers http.Header) (*Response, error) {
	return l.do(http.MethodGet, url, headers, nil)
}

func (l *Leitung) Post(url string, headers http.Header, data interface{}) (*Response, error) {
	return l.do(http.MethodPost, url, headers, data)
}

func (l *Leitung) Put(url string, headers http.Header, data interface{}) (*Response, error) {
	return l.do(http.MethodPost, url, headers, data)
}

func (l *Leitung) Patch(url string, headers http.Header, data interface{}) (*Response, error) {
	return l.do(http.MethodPost, url, headers, data)
}

func (l *Leitung) Delete(url string, headers http.Header) (*Response, error) {
	return l.do(http.MethodPost, url, headers, nil)
}
