package werkbank_httpclient

import "errors"

var (
	ErrFailedToCreateRequest = errors.New("unable to create the http request")
	ErrFailedToParsedRequestBody = errors.New("unable to parse request body")
)