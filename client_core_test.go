package werkbank_httpclient

import (
	"net/http"
	"testing"
)

func TestSetCommonRequestHeaders(t *testing.T){

	//Arrange
	client := Leitung{}
	commonHeaders := http.Header{}
	commonHeaders.Set("Content-Type","application/json")
	commonHeaders.Set("User-Agent","werkbank-client")

	client.SetCommonHeaders(commonHeaders)
	//Act
	client.getFullHeaders(nil)
	//Assert
	if client.CommonHeaders.Get("Content-Type") != "application/json"{
		t.Errorf("expected for Content-Type: application/json")
	}
	if client.CommonHeaders.Get("User-Agent") != "werkbank-client"{
		t.Errorf("expected for User-Agent: werkbank-client")
	}
}

func TestSetCustomHeaders(t *testing.T){
	//Arrange
	client := Leitung{}
	commonHeaders := http.Header{}
	commonHeaders.Set("Content-Type","application/xml")
	client.SetCommonHeaders(commonHeaders)

	requestHeader := http.Header{}
	requestHeader.Set("ABC","123")
	//Act
	headers := client.getFullHeaders(requestHeader)

	//Assert
	if headers.Get("Content-Type") != "application/xml"{
		t.Errorf("expected for Content-Type: application/xml")
	}

	if headers.Get("ABC") != "123"{
		t.Errorf("expected for custom header ABC: 123")
	}
}