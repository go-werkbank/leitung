package werkbank_httpclient

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	"strings"
)

type Response struct {
	StatusCode int
	Headers    http.Header
	Body       []byte
}

func (r *Response) Unmarshal(tp string, obj interface{}) error {
	switch strings.ToLower(tp) {
	case "json":
		return json.Unmarshal(r.Body, &obj)
	case "xml":
		return xml.Unmarshal(r.Body, &obj)
	default:
		return json.Unmarshal(r.Body, &obj)
	}

}
