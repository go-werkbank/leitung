package werkbank_httpclient

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"io"
	"net/http"
	"strings"
)

func (l *Leitung) do(method, url string, headers http.Header, data interface{}) (*Response, error) {

	fullHeaders := l.getFullHeaders(headers)
	requestBody, err := l.getRequestBody(fullHeaders.Get("Content-Type"), data)
	if err != nil {
		return nil, ErrFailedToParsedRequestBody
	}
	request, err := http.NewRequest(method, url, bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, ErrFailedToCreateRequest
	}

	request.Header = fullHeaders

	rsp, err := l.client.Do(request)
	if err != nil {
		return nil, err
	}
	defer rsp.Body.Close()

	body, err := io.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}

	response := Response{
		StatusCode: rsp.StatusCode,
		Headers:    rsp.Header,
		Body:       body,
	}

	return &response, nil

}

func (l *Leitung) getFullHeaders(requestHeaders http.Header) http.Header {

	headers := http.Header{}
	//add common headers
	for key, value := range l.CommonHeaders {
		if len(value) > 0 {
			headers.Set(key, value[0])
		}
	}

	//add custom headers
	for key, value := range requestHeaders {
		if len(value) > 0 {
			headers.Set(key, value[0])
		}
	}

	return headers
}

func (l *Leitung) getRequestBody(contentType string, body interface{}) ([]byte, error) {
	if body == nil {
		return nil, nil
	}

	switch strings.ToLower(contentType) {
	case "application/json":
		return json.Marshal(body)
	case "application/xml":
		return xml.Marshal(body)
	default:
		return json.Marshal(body)
	}
}
