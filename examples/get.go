package examples

import "fmt"

type Endpoint struct {
	CurrentUserUrl string `json:"current_user_url"`
	EmojisUrl      string `json:"emojis_url"`
}

func Get() {
	var ed Endpoint
	response, err := client.Get("https://api.github.com", nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("------------------------------------")
	fmt.Printf("Headers: #%v\n", response.Headers)

	fmt.Println("------------------------------------")

	fmt.Printf("Status: %d\n", response.StatusCode)
	fmt.Println("------------------------------------")

	fmt.Printf("Data: %s\n", response.Body)
	fmt.Println("------------------------------------")
	fmt.Println()
	fmt.Println()
	err = response.Unmarshal("json", &ed)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Printf("URL:%s\n", ed.EmojisUrl)
}
