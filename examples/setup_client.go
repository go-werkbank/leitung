package examples

import (
	"time"

	werkbank_httpclient "gitlab.com/go-werkbank/leitung"
)

var client werkbank_httpclient.Leitung

func GetHttpClient() {
	cfg := werkbank_httpclient.Config{
		MaxIdleConnections: 10,
		ConnectionTimeout:  5 * time.Second,
		ResponseTimeout:    5 * time.Second,
	}
	client = werkbank_httpclient.New(cfg)
}
